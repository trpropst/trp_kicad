# KiCAD Field of Dreams
# This started as a way to add missing fields to components in a schematic.
# The problem arises when you do not start a schematic with template fields 
# set the way you intend in the end.
#
# Features:
# - Search a schematic for a field and report the components that are missing
#   it.
# - Optionally, add the field in the numeric location in the field list for
#   components where the fild is missing.
#
# Issues:
# - There is no means to delete a field. This should be easy enough to add.
# - You need to know where to add a field. If some components are missing
#   several fields, you need to add them, in order, one at a time.
# - If an existing field contains values you want to retain, you cannot simply
#   rename the field.
# - There is no way to specify the value for a new field. This should be easy
#   enough to add.

import argparse
import os
import re

parser = argparse.ArgumentParser(description = "Edit component fields in " +\
	"KiCad schematics.")
parser.add_argument("schematic")
parser.add_argument("-s", dest="field_text", help="Search for a specific field name.")
parser.add_argument("-a", dest="add_field_num", type=int, \
	help="Add missing search results at the desired field position")

args = parser.parse_args()

schematic = open(args.schematic, 'r')

if args.add_field_num:
	if args.add_field_num < 4:
		print "It is a bad idea to edit the default fields (0-3)."
		quit()

	if not os.path.exists("new_schematics"):
		os.mkdir("new_schematics")
	output = open("new_schematics/"+args.schematic, 'w')

if args.field_text:
	in_comp = False
	comp_count = 0
	miss_count = 0
	corr_count = 0
	field_found = False
	field_add = False

	field = '"' + args.field_text + '"'
	print "Searching for: " + field
	for line in schematic:
		if args.add_field_num:
			output.write(line)

		# Copied this from http://stackoverflow.com/a/2787064/2957752
		PATTERN = re.compile(r'''((?:[^ "']|"[^"]*"|'[^']*')+)''')
		data = PATTERN.split(line)[1::2]
	
		# Make sure we're making changes only in component sections to be safe.
		if data[0].strip() == "$Comp":
			in_comp = True
			field_found = False
			field_add = False
			comp_ref = ""
			comp_count += 1
			continue
		if data[0].strip() == "$EndComp":
			if not field_found:
				status = comp_ref + ", Missing field, "
				status += "" if field_add else "Not "
				status += "Added"
				print status
				miss_count += 1
			in_comp = False
			continue
		if in_comp:
			if field_found:
				continue
			if data[0] == 'F' and data[1] == '0':
				comp_ref = data[2]
				continue
			# Is this our field?
			try:
				if data[0] == 'F' and data[10].strip() == field:
					field_found = True
					continue
			except IndexError:
				null_field = True
			
			# If we need to add the field, is this the previous one?
			if args.add_field_num and data[0] == 'F' and \
			data[1] == str(args.add_field_num - 1):
				lastline = line
				lastfield = data
				line = next(schematic)
				data = PATTERN.split(line)[1::2]
	
				try:
					if data[0] == 'F' and data[10].strip() == field:
						field_found = True
						output.write(line)
						continue
				except IndexError:
					null_field = True

				if data[0] == 'F':
					print "There is an existing field where you want to add."
				else:
					print lastfield
					newfield = "F " + str(args.add_field_num) +  " \"Value\" H "
					newfield += lastfield[4] + ' ' + lastfield[5] + ' '
					newfield += "50 0001 L CNN " + field
					output.write(newfield + '\n')
					field_add = True
					corr_count += 1

				output.write(line)

	print "Component count = " + str(comp_count)
	print "Missing fields " + field + " = " + str(miss_count)
	print "Additions made = " + str(corr_count)
